import sys
import signal
import GameMonitor
import GameListParser
import TSCommunicator
import ConfigParser

M_CFG_PATH = "notifier.cfg"


if __name__ == "__main__":
    # define signal handler
    def signal_handler(sig, frame):
        print('Closing..')
        ts_comm.send_message("clientupdate client_nickname=" + game_monitor.m_client_name)
        sys.exit(0)


    # set up signal handler
    signal.signal(signal.SIGINT, signal_handler)

    # read config file
    parameters = ConfigParser.read_config_file(M_CFG_PATH)

    # init ts3 communication module
    ts_comm = None
    try:
        ts_comm = TSCommunicator.TSCommunicator(parameters["host"],
                                                parameters["port"],
                                                parameters["apikey"])
    except Exception:
        quit("failed to connect to TS3 client")

    # parse game list
    game_list_parser = GameListParser.GameListParser(parameters["game_list_file_path"])

    # initiate game monitor
    game_monitor = GameMonitor.GameMonitor(game_list_parser.m_games_dict, ts_comm)

    # monitor for games
    game_monitor.monitor_for_games()

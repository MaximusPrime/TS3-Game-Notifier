

GAME_LIST_SPLIT_CHAR = '$'
GAME_LIST_COMMENT_CHAR = '#'


def read_game_list_file(path):
    """
    reads a file with pairs of process names and their corresponding game,
    and returns a dictionary with the process name as the key, and game name as the value.
    :param path: path to game list file.
    :return: dictionary mapping process names to game name.
    """

    # read the lines
    with open(path) as f:
        content = f.readlines()

    games_dict = {}
    # remove whitespace characters
    content = [x.strip() for x in content]

    for i, line in enumerate(content):
        # skip comments and newlines
        if len(line) == 0 or line[0] == GAME_LIST_COMMENT_CHAR:
            continue

        split_line = line.split(GAME_LIST_SPLIT_CHAR)
        if len(split_line) != 2:
            print "error at line (" + str(i) + ") in game list: no split char"
            continue

        # add entry to dictionary, and strip whitespace characters
        games_dict[split_line[0].lower().strip()] = split_line[1].strip()

    return games_dict


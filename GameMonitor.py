import psutil
import time


class GameMonitor(object):

    def __init__(self, game_dict, ts_comms):
        self.m_game_dict = game_dict
        self.m_ts_comms = ts_comms
        self.m_client_name = self.get_client_name()

    def find_games_in_processes(self):
        """
        search for games that are running currently.
        :return: list of games that are currently running.
        """
        process_names = []
        games = []

        for p in psutil.pids():
            process_names.append(psutil.Process(p).name())

        for game_proc_name in self.m_game_dict.keys():
            for proc in process_names:
                if game_proc_name in proc:
                    games.append(self.m_game_dict[game_proc_name])

        return games

    def monitor_for_games(self, sleep_time=10):
        """
        monitors for games running the in the background.
        if a new game is detected then the client nickname is updated.

        :param sleep_time: time in seconds between probes.
        :return: None
        """
        current_client_name = self.m_client_name
        while True:
            games = self.find_games_in_processes()
            if len(games) > 0:
                new_client_name = self.m_client_name + "\sis\splaying\s" + self.games_to_str(games)
            else:
                new_client_name = self.m_client_name

            if current_client_name != new_client_name:
                self.m_ts_comms.send_message("clientupdate client_nickname=" + new_client_name)
                current_client_name = new_client_name

            # sleep till next probe
            time.sleep(sleep_time)

    def get_client_name(self):
        """
        queries the ts3 client and fetches the client nickname.
        :return: current client nickname
        """

        ''' whoami repsponds with:
                            clid=NUM1 CID=NUM2 '''
        whoami_resp = self.m_ts_comms.send_command_and_parse("whoami")

        ''' clientvariable clid=NUM1 PROPERTY1 PROPERTY2 ... responds with:
                            clid=NUM1 PROPERTY1=VAL1 PROPERTY2=VAL2 ... '''
        command = "clientvariable clid=" + whoami_resp["clid"] + " client_nickname"
        clientvariable_resp = self.m_ts_comms.send_command_and_parse(command)

        return clientvariable_resp["client_nickname"]

    @staticmethod
    def games_to_str(games_list):
        """
        creates string of a list of games.
        if the list contains one game its name is returned.
        if the list contains more than one game, they are separated by commas.

        :param games_list: list of games.
        :return: string as described above.
        """
        if len(games_list) == 1:
            return games_list[0]
        return ',\s'.join(games_list)

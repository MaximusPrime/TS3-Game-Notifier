# TS3-Game-Notifier

A little program to change your TS3 nickname to show others what game you're currently playing.
It actively monitors your running processes and looks for games you've defined.  
When a game is detected your nickname will change to reflect that.
   
E.g. my nickname is *TheDude*, and I'm playing *Rocket League*.  
So my nickname will change to *TheDude is playing Rocket League*.

#### To run:
```
python Notifier.py
```

#### dependencies
* psutil

to install:
```
pip install psutil
```

#### notifier.cfg
This file contains basic configuration for the program.  
Needs to be located in the same folder as Notifier.py  

#### Parameters:
* host - _IP address where the TS3 client is running. Usually your own machine._
* port - _Port of the ClientQuery plugin._
* apikey - _API key from ClientQuery plugin._
* game_list_file_path - _Path of game list file._

#### games_list.txt
This is a file that contains lines that maps a games' process name to its actual name.  
The format is:
```
game_process_name $ actual_game_name
```
For example:
```
RainbowSix $ Rainbow Six Siege
```
Notes:
* The process name does not have to contain an .exe suffix.
* Capitalization is important. It's best to copy the process name directly from the games' folder.
* _actual_game_name_ can be anything. This is what will be displayed in the TS3 client. 
* Check out games_list.txt file for an example.
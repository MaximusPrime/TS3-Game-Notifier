import telnetlib
import time


class TSCommunicator(object):

    def __init__(self, host, port, api_key, sleep_time=1):
        self.m_host = host
        self.m_port = port
        self.m_api_key = api_key
        self.m_sleep_time = sleep_time

        self.m_telnet = telnetlib.Telnet(host=self.m_host, port=self.m_port)
        time.sleep(1)
        self.m_telnet.read_very_eager()

        # authenticate
        self.send_message("auth apikey=" + self.m_api_key)

    def send_message(self, message):
        """
        sends a given message to the ClientQuery plugin using Telnet protocol, and returns the response.
        time.sleep() is called to allow the plugin respond reliably.

        :param message: a given message.
        :return: the response.
        """
        time.sleep(self.m_sleep_time)
        self.m_telnet.write(message + '\r\n')

        time.sleep(self.m_sleep_time)
        resp = self.m_telnet.read_eager()
        return resp

    def send_command_and_parse(self, command):
        """
        sends a given command, and returns a dictionary describing the response.
        the dictionary contains variables as keys, and their corresponding values.

        :param command: a given command.
        :return: dictionary of the response as described above.
        """
        fields = {}

        print "sending.. " + command
        response = self.send_message(command).split()
        print "raw response = " + str(response)

        for val in response:
            split_val = val.split('=')
            if len(split_val) != 2:
                continue
            fields[split_val[0]] = split_val[1]

        return fields




M_CFG_SPLIT_CHAR = '='
M_CFG_COMMENT_CHAR = '#'


def read_config_file(path):
    """
    read config file, and return a dictionary with predefined keys, and their values.
    the keys are the parameters.

    Note: the keys must match all parameters in the file. Any parameter that is missing from the dictionary defined
    in the function will be ignored.

    :param path: path of the config file.
    :return: dictionary with keys as parameters and their corresponding values.
    """
    parameters = {"host": None,
                  "port": None,
                  "apikey": None,
                  "game_list_file_path": None}

    # read file
    with open(path) as f:
        content = f.readlines()

    # remove whitespace characters
    content = [x.strip() for x in content]

    for i, line in enumerate(content):
        # skip comments and newlines
        if len(line) == 0 or line[0] == M_CFG_COMMENT_CHAR:
            continue

        split_line = line.split(M_CFG_SPLIT_CHAR)
        if len(split_line) != 2:
            print "error at line (" + str(i) + ") in game list: no split char"
            continue

        parameters[split_line[0].strip()] = split_line[1].strip()

    return parameters
